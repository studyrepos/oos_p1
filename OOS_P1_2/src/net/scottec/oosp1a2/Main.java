package net.scottec.oosp1a2;

/**
 * Main P1 A2
 *
 * @author Fabian Schöttler
 */
public class Main {

  /**
   * The MainClass
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {

    Point p1 = new Point(5, 5);
    KreisVererb c1 = new KreisVererb(7, 7, 3);
    KreisVererb c2 = new KreisVererb(p1, 5);
    KreisVererb c3 = new KreisVererb(7, 7, 2);
    KreisVererb c4 = new KreisVererb(7, 7, 3);

    System.out.println("Punkt p1: " + p1.toString());
    System.out.println("Kreis c1: " + c1.toString());
    System.out.println("Kreis c2: " + c2.toString());
    System.out.println("Kreis c3: " + c3.toString());
    System.out.println("Kreis c4: " + c4.toString());

    System.out.println("c1 equals c2: " + c1.equals(c2));
    System.out.println("c1 equals c3: " + c1.equals(c3));
    System.out.println("c1 equals c4: " + c1.equals(c4));
  }
}
