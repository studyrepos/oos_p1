package net.scottec.oosp1a2;

/**
 * Point
 *
 * @author Fabian Schöttler
 */
public class Point {
  /**
   * Initiates new Point with zero attribiutes
   */
  public Point() {
    this.x = 0;
    this.y = 0;
  }
  /**
   * CopyConstructor
   *
   * @param _p Point to copy
   */
  public Point(Point _p) {
    this.x = _p.x;
    this.y = _p.y;
  }

  /**
   * PointConstructor from coordinates
   *
   * @param _x x-coordinate
   * @param _y y-coordinate
   */
  public Point(int _x, int _y) {
    this.x = _x;
    this.y = _y;
  }

  /**
   * Returns point by copyconstructor
   *
   * @return Point
   */
  public Point getLocation() {
    return new Point(this);
  }

  /**
   * Set location to given Point location
   *
   * @param _p Point to set location
   */
  public void setLocation(Point _p) {
    this.x = _p.x;
    this.y = _p.y;
  }

  /**
   * Set location to given coordinates
   *
   * @param _x x-coordinates
   * @param _y y-coordinates
   */
  public void setLocation(int _x, int _y) {
    this.x = _x;
    this.y = _y;
  }

  /**
   * Move Point by given deltas
   *
   * @param _dx delta for x-coordinate
   * @param _dy delta for y-coordinate
   */
  public void move(int _dx, int _dy) {
    this.x += _dx;
    this.y += _dy;
  }

  /**
   * Compare to given Point
   *
   * @param _p Point to compare with
   * @return True if congruent coordinates, else False
   */
  public boolean equals(Point _p) {
    return ((this.x == _p.x) && (this.y == _p.y));
  }

  /**
   * Represent Point as String
   *
   * @return (x:y)
   */
  public String toString() {
    return "(" + this.x + ":" + this.y + ")";
  }

  // x-Coord (cart)
  private int x;
  // y-Coord (cart)
  private int y;
}
