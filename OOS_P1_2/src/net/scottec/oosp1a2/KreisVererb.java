package net.scottec.oosp1a2;

/**
 * KreisVererb, inheritates from Point as center
 *
 * @author Fabian Schöttler
 */
public class KreisVererb extends Point {

  /**
   * Default Constructor
   */
  public KreisVererb() {
    super();
    this.radius = 0;
  }

  /**
   * Constructor with CenterPoint with zero radius
   *
   * @param _center CenterPoint
   */
  public KreisVererb(Point _center) {
    super(_center);
    this.radius = 0;
  }

  /**
   * Constructor with coordinates
   *
   * @param _center_x x-coordinate for center
   * @param _center_y y-coordinate for center
   */
  public KreisVererb(int _center_x, int _center_y) {
    super(_center_x, _center_y);
    this.radius = 0;
  }

  /**
   * Constructor with centerPoint and radius
   *
   * @param _center center for new circle
   * @param _radius radius for new circle
   */
  public KreisVererb(Point _center, int _radius) {
    super(_center);
    this.radius = _radius;
  }

  /**
   * Constructor with coordinates for center and radius
   *
   * @param _center_x x-coordinate for center
   * @param _center_y y-coordinate for center
   * @param _radius   radius for new circle
   */
  public KreisVererb(int _center_x, int _center_y, int _radius) {
    super(_center_x, _center_y);
    this.radius = _radius;
  }

  /**
   * Returns radius of circle
   *
   * @return Radius
   */
  public int getRadius() {
    return this.radius;
  }

  /**
   * Sets radius of circle
   *
   * @param _radius radius to be set
   */
  public void setRadius(int _radius) {
    if (_radius >= 0) {
      this.radius = _radius;
    }
  }

  /**
   * Compares to given circle
   *
   * @param _circle circle to compare with
   * @return True, if congruent centers and equal radius, else False
   */
  public boolean equals(KreisVererb _circle) {
    return (super.equals(_circle.getLocation()) && this.radius == _circle.radius);

  }

  /**
   * String representation of circle
   *
   * @return String
   */
  public String toString() {
    return "[C-" + super.toString() + "-" + this.radius + "]";
  }

  // radius of circle
  private int radius;
}
