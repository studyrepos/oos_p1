package net.scottec.oosp1a3;

/**
 * Rectangle, implementing IGeoForm
 *
 * @author Fabian Schöttler
 */
public class Rechteck implements IGeoForm  {

  /**
   * DefaultConstructor
   */
  public Rechteck() {
  }
  /**
   * Constructor with corners
   *
   * @param _upperLeft  upper left corner
   * @param _lowerRight lower right corner
   */
  public Rechteck(Point _upperLeft, Point _lowerRight) {
    this.cornerUpperLeft = _upperLeft.getLocation();
    this.cornerLowerRight = _lowerRight.getLocation();
  }

  /**
   * Calculates area
   *
   * @return area as double
   */
  public double flaechenInhalt() {
    return (this.cornerUpperLeft.getDistanceX(this.cornerLowerRight)
        * this.cornerUpperLeft.getDistanceY(this.cornerLowerRight));
  }

  /**
   * Compare to given rectangle
   *
   * @param _rechteck rectangle to compare with
   * @return True, if congruent corners, else False
   */
  public boolean equals(IGeoForm _rechteck) {
    return ( _rechteck instanceof Rechteck
        && this.cornerUpperLeft.equals(
            ((Rechteck)_rechteck).cornerUpperLeft)
        && this.cornerLowerRight.equals(
            ((Rechteck)_rechteck).cornerLowerRight));
  }

  /**
   * Clone Rectangle
   *
   * @return new rectangle from attributes
   */
  public Object clone() {
    return new Rechteck(this.cornerUpperLeft, this.cornerLowerRight);
  }

  /**
   * String representation of rectangle
   *
   * @return string
   */
  public String toString() {
    return "[R-" + this.cornerUpperLeft.toString()
        + ":" + this.cornerLowerRight.toString() + "]";
  }

  // upper left corner
  private Point cornerUpperLeft = new Point();
  // lower right corner
  private Point cornerLowerRight = new Point();
}
