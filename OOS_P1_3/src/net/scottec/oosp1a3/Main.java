package net.scottec.oosp1a3;

/**
 * Main P1 A3
 *
 * @author Fabian Schöttler
 */
public class Main {

  public static void main(String[] args) {

    Point p1 = new Point(1, 1);
    Point p2 = new Point(4, 4);
    Point p3 = new Point(7, 7);

    Rechteck r1 = new Rechteck(p1, p2);
    Rechteck r2 = new Rechteck(p1, p3);
    Rechteck r3 = new Rechteck(p1, p2);

    KreisAgg c1 = new KreisAgg(p1, 4);
    KreisAgg c2 = new KreisAgg(p2, 6);
    KreisAgg c3 = new KreisAgg(p1, 4);

    System.out.println("Punkt p1: " + p1.toString());
    System.out.println("Punkt p2: " + p2.toString());
    System.out.println("Punkt p3: " + p3.toString());

    System.out.println("Rechteck r1: " + r1.toString()
        + " | Fläche: " + r1.flaechenInhalt());
    System.out.println("Rechteck r2: " + r2.toString()
        + " | Fläche: " + r2.flaechenInhalt());
    System.out.println("Rechteck r3: " + r3.toString()
        + " | Fläche: " + r3.flaechenInhalt());

    System.out.println("Kreis c1: " + c1.toString()
        + " | Fläche: " + c1.flaechenInhalt());
    System.out.println("Kreis c2: " + c2.toString()
        + " | Fläche: " + c2.flaechenInhalt());
    System.out.println("Kreis c3: " + c3.toString()
        + " | Fläche: " + c3.flaechenInhalt());

    System.out.println("r1 equals r2: " + r1.equals(r2));
    System.out.println("r1 equals r3: " + r1.equals(r3));
    System.out.println("c1 equals c2: " + c1.equals(c2));
    System.out.println("c1 equals c3: " + c1.equals(c3));

    System.out.println("r1 equals c3: " + r1.equals(c3));

    IGeoForm[] forms = new IGeoForm[4];

    forms[0] = (IGeoForm) r1.clone();
    forms[1] = (IGeoForm) r2.clone();
    forms[2] = (IGeoForm) c1.clone();
    forms[3] = (IGeoForm) c2.clone();

    // print forms, calculate cummulated area
    double areaAll = 0;
    for (int i = 0; i < forms.length; i++) {
      areaAll += forms[i].flaechenInhalt();
      System.out.println("forms[" + i + "]: " + forms[i].toString()
          + " | Fläche: " + forms[i].flaechenInhalt());
    }
    System.out.println("Gesamtfläche der Forms: " + areaAll);
  }
}
