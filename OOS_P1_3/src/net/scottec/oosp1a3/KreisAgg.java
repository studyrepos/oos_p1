package net.scottec.oosp1a3;

/**
 * Circle, implementing IGeoForm
 *
 * @author Fabian Schöttler
 */
public class KreisAgg implements IGeoForm {

  /**
   * Default constructor
   */
  public KreisAgg() {
  }
  /**
   * Constructor for center with zero radius
   *
   * @param _center center
   */
  public KreisAgg(Point _center) {
    this.center = _center.getLocation();
  }

  /**
   * Constructor for center and radius
   *
   * @param _center center
   * @param _radius radius
   */
  public KreisAgg(Point _center, int _radius) {
    this(_center);
    this.radius = _radius;
  }

  /**
   * Calculates area
   *
   * @return area as double
   */
  public double flaechenInhalt() {
    return (Math.PI * this.radius * this.radius);
  }

  /**
   * Compare to given circle
   *
   * @param _circle circle to compare with
   * @return True, if congruent center and radius, else False
   */
  public boolean equals(IGeoForm _circle) {
    return (_circle instanceof KreisAgg
        && this.center.equals(((KreisAgg)_circle).center)
        && this.radius == ((KreisAgg)_circle).radius);
  }

  /**
   * Clone circle
   *
   * @return new circle from attributes
   */
  public Object clone() {
    return new KreisAgg(this.center, this.radius);
  }

  /**
   * String representation of circle
   *
   * @return string
   */
  public String toString() {
    return "[C-" + this.center.toString() + "-" + this.radius + "]";
  }

  // center of circle
  private Point center = new Point();
  // radius of circle
  private int radius = 0;
}
