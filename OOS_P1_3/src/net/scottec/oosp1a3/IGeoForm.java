package net.scottec.oosp1a3;

public interface IGeoForm {
  double flaechenInhalt();

  String toString();

  boolean equals(IGeoForm _form);
}
