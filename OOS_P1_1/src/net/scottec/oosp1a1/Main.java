package net.scottec.oosp1a1;

/**
 * Main P1 A1
 *
 * @author Fabian Schöttler
 */
public class Main {

  /**
   * The MainClass
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    final int depth = 20;
    calcPascalTriangle(depth);
  }


  /**
   * Calculates the PascalTriangle to the given depth.
   *
   * @param _depth maximum depth to calculate
   */
  private static void calcPascalTriangle(int _depth) {
    // declare 2D array, init only first dimension
    int[][] triangle = new int[_depth][];

    // iterate over rows
    for (int row = 0; row < _depth; row++) {
      // init row, second dimension of 2D array
      triangle[row] = new int[row + 1];
      // set outer elements
      triangle[row][0] = 1;
      triangle[row][row] = 1;
      // print row index
      System.out.print("[" + row + "] : ");
      // output left outer element, only for rows > 0
      if (row > 0) {
        System.out.print("  " + triangle[row][0]);
      }
      // calculate inner elements for rows > 2
      for (int col = 1; col < row; col++) {
        // calculate element for this col
        triangle[row][col] = triangle[row - 1][col - 1] + triangle[row - 1][col];
        // output col element
        System.out.print("  " + triangle[row][col]);
      }
      // output for right outer element
      System.out.println("  " + triangle[row][row]);
    }
  }
}
